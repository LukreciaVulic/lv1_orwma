package com.example.famousscientists;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    TextView tvMarieCurieNaslov, tvMarieCurieGodine, tvMarieCurie, tvMaxwellNaslov,tvMaxwellGodine, tvMaxwell, tvHawkingNaslov, tvHawkingGodine, tvHawking, tvNaslov;
    Button btnEditDes, btnInspo;
    EditText etDescription;
    ImageView ivMarieCurie, ivMaxwell, ivHawking;
    RadioButton rbtnMarieCurie, rbtnMaxwell, rbtnHawking;
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar;
        actionBar = getSupportActionBar();

        ColorDrawable colorDrawable
                = new ColorDrawable(Color.parseColor("#000000"));

        // Set BackgroundDrawable
        actionBar.setBackgroundDrawable(colorDrawable);
        setContentView(R.layout.activity_main);
        this.Initilaze();

    }
    

    private void Initilaze(){
        this.btnEditDes=(Button)findViewById(R.id.btnEditDescriprion);
        this.btnEditDes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String description=(String)etDescription.getText().toString();
                if (rbtnHawking.isChecked()) {
                    tvHawking.setText(description);
                    rbtnHawking.setChecked(false);
                }
                else if(rbtnMarieCurie.isChecked()) {
                    tvMarieCurie.setText(description);
                    rbtnMarieCurie.setChecked(false);
                }
                else if (rbtnMaxwell.isChecked()) {
                    tvMaxwell.setText(description);
                    rbtnMaxwell.setChecked(false);
                }
            }
        });

        this.btnInspo=(Button)findViewById(R.id.btnInspiration);
        this.btnInspo.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                String[] randomStrings = new String[] {"Hawking was diagnosed with motor neuron disease, more commonly known as Lou Gehrig's disease","In the year 1864, James Clerk Maxwell proposed his “Dynamical Theory of the Electromagnetic Field”","In order to fit in, she changed her name from Manya to Marie.","Curie died on July 4, 1934, of aplastic anemia, believed to be caused by prolonged exposure to radiation."};
                Toast.makeText(getApplicationContext(),randomStrings[new Random().nextInt(randomStrings.length - 1)],Toast.LENGTH_LONG).show();
            }
        });

        this.etDescription=(EditText)findViewById(R.id.etDescription);

        this.tvHawking=(TextView)findViewById(R.id.tvHawking);
        this.tvHawkingNaslov=(TextView)findViewById(R.id.tvHawkingNaslov);
        this.tvHawkingGodine=(TextView)findViewById(R.id.tvHawkingGodine);

        this.tvMarieCurie=(TextView)findViewById(R.id.tvMarieCurie);
        this.tvMarieCurieNaslov=(TextView)findViewById(R.id.tvMarieCurieNaslov);
        this.tvMarieCurieGodine=(TextView)findViewById(R.id.tvMarieCurieGodine);

        this.tvMaxwell=(TextView)findViewById(R.id.tvMaxwell);
        this.tvMaxwellNaslov=(TextView)findViewById(R.id.tvMaxwellNaslov);
        this.tvMaxwellGodine=(TextView)findViewById(R.id.tvMaxwellGodine);

        this.ivHawking=(ImageView)findViewById(R.id.ivHawking);
        this.ivHawking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivHawking.setVisibility(View.INVISIBLE);
            }
        });
        this.ivMarieCurie=(ImageView)findViewById(R.id.ivMarieCurie);
        this.ivMarieCurie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivMarieCurie.setVisibility(View.INVISIBLE);
            }
        });
        this.ivMaxwell=(ImageView)findViewById(R.id.ivMaxwell);
        this.ivMaxwell.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivMaxwell.setVisibility(View.INVISIBLE);
            }
        });

        this.rbtnHawking=(RadioButton)findViewById(R.id.rBtnHawking);
        this.rbtnMarieCurie=(RadioButton)findViewById(R.id.rBtnCurie);
        this.rbtnMaxwell=(RadioButton)findViewById(R.id.rBtnMaxwell);
    }


}